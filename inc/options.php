<?php
//adding setting tuong tac
add_action('customize_register', function ($wp_customize){
    //adding section in wordpress customizer
    $wp_customize->add_section('tuong_tac_extras_section', array(
        'title'          => 'Tuỳ chỉnh icon tương tác'
    ));
    // bat tat icon tren may tinh
    $wp_customize->add_setting( 'desktop_checkbox',
        array(
            'default' => 1,
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_attr'
        )
    );

    $wp_customize->add_control( 'desktop_checkbox',
        array(
            'label' => __( 'Icon tương tác Desktop', 'ephemeris' ),
            'description' => esc_html__( 'Bật tắt icon trên Desktop' ),
            'section'  => 'tuong_tac_extras_section',
            'priority' => 1,
            'type'=> 'checkbox',
            'capability' => 'edit_theme_options',
        )
    );

    // bat tat icon tren mobile
    $wp_customize->add_setting( 'mobile_checkbox',
        array(
            'default' => 1,
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_attr'
        )
    );

    $wp_customize->add_control( 'mobile_checkbox',
        array(
            'label' => __( 'Icon tương tác mobile', 'ephemeris' ),
            'description' => esc_html__( 'Bật tắt icon trên mobile' ),
            'section'  => 'tuong_tac_extras_section',
            'priority' => 2,
            'type'=> 'checkbox',
            'capability' => 'edit_theme_options',
        )
    );
    // tuy chinh mess facebook
    $wp_customize->add_setting( 'options_facebook_text',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'sanitize_url'
        )
    );

    $wp_customize->add_control( 'options_facebook_text',
        array(
            'label' => __( 'Link chat facebook' ),
            'description' => esc_html__( 'Link facebook fanpage' ),
            'section' => 'tuong_tac_extras_section',
            'priority' => 3, // Optional. Order priority to load the control. Default: 10
            'type' => 'url', // Can be either text, email, url, number, hidden, or date
            'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
            'input_attrs' => array( // Optional.
                'class' => '',
                'placeholder' => __( 'http://m.me/devqanh' ),
            ),
        )
    );


    // tuy chinh zalo
    $wp_customize->add_setting( 'options_zalo_text',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control( 'options_zalo_text',
        array(
            'label' => __( 'Link chat zalo' ),
            'description' => esc_html__( 'Link zalo' ),
            'section' => 'tuong_tac_extras_section',
            'priority' => 4, // Optional. Order priority to load the control. Default: 10
            'type' => 'text', // Can be either text, email, url, number, hidden, or date
            'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
            'input_attrs' => array( // Optional.
                'class' => '',
                'placeholder' => __( '0327239691' ),
            ),
        )
    );
    // tuy chinh mail
    $wp_customize->add_setting( 'options_email_text',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'sanitize_email'
        )
    );

    $wp_customize->add_control( 'options_email_text',
        array(
            'label' => __( 'Email' ),
            'description' => esc_html__( 'Email' ),
            'section' => 'tuong_tac_extras_section',
            'priority' => 3, // Optional. Order priority to load the control. Default: 10
            'type' => 'email', // Can be either text, email, url, number, hidden, or date
            'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
            'input_attrs' => array( // Optional.
                'class' => '',
                'placeholder' => __( 'info@etado.vn' ),
            ),
        )
    );
    // tuy chinh goi dien
    $wp_customize->add_setting( 'options_hotline_text',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control( 'options_hotline_text',
        array(
            'label' => __( 'Link Hotline' ),
            'description' => esc_html__( 'Hotline' ),
            'section' => 'tuong_tac_extras_section',
            'priority' => 5, // Optional. Order priority to load the control. Default: 10
            'type' => 'text', // Can be either text, email, url, number, hidden, or date
            'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
            'input_attrs' => array( // Optional.
                'class' => '',
                'placeholder' => __( '0327239691' ),
            ),
        )
    );
});
