<?php
/*
Plugin Name: Plugin Tương Tác
Plugin URI: http://www.etado.vn
Description: plugin tuong tac
Version: 1.0
Author: DEV
*/
define( 'TUONG_TAC_VERSION', '1.0' );
require_once('inc/options.php');
// include file css

add_action( 'wp_enqueue_scripts', function (){
    wp_register_style( 'tuongtac-style', plugin_dir_url( __FILE__ ) . 'assets/css/style.css', array(), TUONG_TAC_VERSION );
    wp_enqueue_style( 'tuongtac-style' );
} );

// turn on/off icon
if (get_theme_mod('desktop_checkbox') && !wp_is_mobile()) :
    add_action('wp_footer', 'template_tuong_tac_qa');
elseif (wp_is_mobile() && get_theme_mod('mobile_checkbox')) :
    add_action('wp_footer', 'template_tuong_tac_qa');
endif;
// template tuong tac
function template_tuong_tac_qa()
{
    ob_start();

    ?>
    <div class="tuong-tac-qa">
        <ul class="icon-contact-page">
            <?php if (get_theme_mod('options_facebook_text')): ?>
                <li><a href="<?php echo get_theme_mod('options_facebook_text'); ?>" target="_blank"
                       rel="noopener noreferrer"><img
                                src="<?php echo esc_url(plugins_url('assets/img/facebook.png', __FILE__)); ?>"
                                alt=""></a>
                </li>
            <?php endif; ?>
            <?php if (get_theme_mod('options_zalo_text')): ?>
                <li><a href="https://zalo.me/<?php echo get_theme_mod('options_zalo_text'); ?>" target="_blank"><img
                                src="<?php echo esc_url(plugins_url('assets/img/zalo.png', __FILE__)); ?>" alt="" ></a>
                </li>
            <?php endif; ?>
            <?php if (get_theme_mod('options_email_text')): ?>
                <li><a href="mailto:<?php echo get_theme_mod('options_email_text'); ?>"><img
                                src="<?php echo esc_url(plugins_url('assets/img/mail-send.png', __FILE__)); ?>" alt=""></a>
                </li>
            <?php endif; ?>
            <?php if (get_theme_mod('options_hotline_text')): ?>
                <li><a href="tel:<?php echo get_theme_mod('options_hotline_text'); ?>"><img
                                src="<?php echo esc_url(plugins_url('assets/img/phone-call.png', __FILE__)); ?>" alt=""></a>
                </li>
            <?php endif; ?>
        </ul>
    </div>

    <?php
    echo ob_get_clean();
}